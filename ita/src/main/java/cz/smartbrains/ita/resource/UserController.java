package cz.smartbrains.ita.resource;

import cz.smartbrains.ita.model.UserRequestDto;
import cz.smartbrains.ita.model.UserResponseDto;
import cz.smartbrains.ita.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * UserController
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserServiceImpl userService;

    @GetMapping
    public List<UserResponseDto> findAll() {
        return userService.findAll();
    }

    @PostMapping
    public UserResponseDto createUser(@RequestBody @Valid final UserRequestDto requestDto) {
        return userService.createUser(requestDto);
    }

    @PutMapping
    public UserResponseDto updateUser(@RequestBody @Valid final UserRequestDto requestDto) {
        return userService.updateByLogin(requestDto);
    }

    @DeleteMapping("/{login}")
    public void deleteUser(@PathVariable("login") final String login) {
        userService.deleteByLogin(login);
    }

    @GetMapping("/{login}")
    public UserResponseDto findByLogin(@PathVariable("login") final String login) {
        return userService.findByLogin(login);
    }
}
