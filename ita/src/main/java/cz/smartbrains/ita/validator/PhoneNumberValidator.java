package cz.smartbrains.ita.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * PhoneNumberValidator
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber, String> {


    @Override
    public boolean isValid(String number, ConstraintValidatorContext constraintValidatorContext) {
        return number != null && number.matches("^(\\+420|)[0-9]{9}$");
    }
}
