package cz.smartbrains.ita.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * User
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
@Data
@AllArgsConstructor
public class UserResponseDto {
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String login;
    private LocalDateTime createdAt;
}
