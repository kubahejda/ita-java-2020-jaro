package cz.smartbrains.ita.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * ExceptionResponse
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
@Data
@AllArgsConstructor
public class ExceptionResponseDto {
    private String message;
    private int status;
}
