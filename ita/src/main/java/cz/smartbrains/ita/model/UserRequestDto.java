package cz.smartbrains.ita.model;

import cz.smartbrains.ita.validator.PhoneNumber;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

/**
 * User
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
@Data
public class UserRequestDto {
    @Size(max = 20)
    private String firstName;
    @Size(max = 20)
    private String lastName;
    @Email
    private String email;
    @PhoneNumber
    private String phone;
    @Size(max = 20)
    private String login;
}
