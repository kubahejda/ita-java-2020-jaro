package cz.smartbrains.ita.service;

import cz.smartbrains.ita.model.UserRequestDto;
import cz.smartbrains.ita.model.UserResponseDto;

import java.util.List;

/**
 * UserService
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
public interface UserService {
    List<UserResponseDto> findAll();

    UserResponseDto findByLogin(String login);

    UserResponseDto updateByLogin(UserRequestDto requestDto);

    UserResponseDto createUser(UserRequestDto requestDto);

    void deleteByLogin(String login);
}
