package cz.smartbrains.ita.service.impl;

import cz.smartbrains.ita.domain.User;
import cz.smartbrains.ita.exception.UserNotFoundException;
import cz.smartbrains.ita.model.UserRequestDto;
import cz.smartbrains.ita.model.UserResponseDto;
import cz.smartbrains.ita.repository.UserRepository;
import cz.smartbrains.ita.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * UserService
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public List<UserResponseDto> findAll() {
        log.info("Called findAll");

        final List<UserResponseDto> response = StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .map(this::mapToDto)
                .collect(Collectors.toList());

        log.debug("Response was: {}", response);
        return response;
    }

    @Override
    public UserResponseDto findByLogin(final String login) {
        log.info("Called findByLogin");
        log.debug("Called findByLogin with login {}", login);
        final User user = userRepository.findByLogin(login)
                .orElseThrow(() -> prepareUserNotFoundException(login));

        final UserResponseDto result = mapToDto(user);

        log.debug("Response was {}", result);
        return result;
    }

    @Override
    public UserResponseDto updateByLogin(final UserRequestDto requestDto) {
        log.info("Called updateByLogin");
        log.debug("Called updateByLogin with request {}", requestDto);
        final User user = userRepository.findByLogin(requestDto.getLogin())
                .orElseThrow(() -> prepareUserNotFoundException(requestDto.getLogin()));

        user.setLogin(requestDto.getLogin())
                .setLastName(requestDto.getLastName())
                .setEmail(requestDto.getEmail())
                .setPhone(requestDto.getPhone())
                .setFirstName(requestDto.getFirstName());

        userRepository.save(user);

        final UserResponseDto result = mapToDto(user);

        log.debug("Response was {}", result);
        return result;
    }

    @Override
    public UserResponseDto createUser(final UserRequestDto requestDto) {
        log.info("Called createUser");
        log.debug("Called createUser with request {}", requestDto);
        final User user = mapToDomain(requestDto);

        userRepository.save(user);

        final UserResponseDto result = mapToDto(user);

        log.debug("Response was {}", result);
        return result;
    }

    @Override
    public void deleteByLogin(final String login) {
        log.info("Called deleteByLogin");
        log.debug("Called deleteByLogin with login {}", login);
        userRepository.deleteByLogin(login);
    }

    private User mapToDomain(final UserRequestDto requestDto) {
        return new User()
                .setEmail(requestDto.getEmail())
                .setFirstName(requestDto.getFirstName())
                .setLastName(requestDto.getLastName())
                .setLogin(requestDto.getLogin())
                .setPhone(requestDto.getPhone());
    }

    private UserResponseDto mapToDto(final User domain) {
        return new UserResponseDto(
                domain.getFirstName(),
                domain.getLastName(),
                domain.getEmail(),
                domain.getPhone(),
                domain.getLogin(),
                domain.getCreatedAt()
        );
    }

    private UserNotFoundException prepareUserNotFoundException(String login) {
        return new UserNotFoundException("Could not find user by login " + login);
    }
}
