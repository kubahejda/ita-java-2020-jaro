package cz.smartbrains.ita.repository;

import cz.smartbrains.ita.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * UserRepository
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByLogin(final String login);

    void deleteByLogin(final String login);
}
