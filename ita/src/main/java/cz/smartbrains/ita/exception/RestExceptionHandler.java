package cz.smartbrains.ita.exception;

import cz.smartbrains.ita.model.ExceptionResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * ExceptionHandler
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ItaException.class})
    protected ResponseEntity<Object> handleItaException(ItaException ex, WebRequest request) {
        log.error("{}. Stacktrace: {}", ex.getMessage(), ex.getStackTrace());
        return handleExceptionInternal(ex, buildResponse(ex),
                new HttpHeaders(), ex.getStatus(), request);
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleInternalServerErrorException(Exception ex, WebRequest request) {
        log.error("{}. Stacktrace: {}", ex.getMessage(), ex.getStackTrace());
        return handleExceptionInternal(ex, buildResponse(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR),
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException exception,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        return new ResponseEntity(new ExceptionResponseDto(exception.getMessage(), status.value()), headers, status);
    }

    private ExceptionResponseDto buildResponse(ItaException exception) {
        return new ExceptionResponseDto(exception.getMessage(), exception.getStatus().value());
    }

    private ExceptionResponseDto buildResponse(String message, HttpStatus status) {
        return new ExceptionResponseDto(message, status.value());
    }
}
