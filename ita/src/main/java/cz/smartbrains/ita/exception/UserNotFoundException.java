package cz.smartbrains.ita.exception;

import org.springframework.http.HttpStatus;

/**
 * UserNotFoundException
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
public class UserNotFoundException extends ItaException {
    public UserNotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }
}
