package cz.smartbrains.ita.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * ItaException
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2020
 */
@Data
public class ItaException extends RuntimeException {
    private HttpStatus status;
    private String message;

    public ItaException() {
        super();
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
        this.message = "Unknown error";
    }

    public ItaException(String message, HttpStatus status) {
        super(message);
        this.status = status;
        this.message = message;
    }

    public ItaException(String message, HttpStatus status, Throwable cause) {
        super(message, cause);
        this.status = status;
        this.message = message;
    }
}
